<?php

add_action('wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles()
{
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

    if (is_rtl()) {
        wp_enqueue_style('salient-rtl', get_template_directory_uri(). '/rtl.css', array(), '1', 'screen');
    }
}

add_filter('woocommerce_add_to_cart_redirect', 'salient_add_to_cart_redirect');
function salient_add_to_cart_redirect()
{
    global $woocommerce;
    $checkout_url = wc_get_checkout_url();
    return $checkout_url;
}

//Add New Pay Button Text
add_filter('woocommerce_product_single_add_to_cart_text', 'salient_cart_button_text');
function salient_cart_button_text()
{
    return __('Pay Now', 'woocommerce');
}
